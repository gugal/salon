# Salon
> ⚠️ This library is experimental.

Compose settings UI components from Android 14 as a library.

## What?
Starting from Android 14, Google has been rewriting the Settings app using Jetpack Compose. The rewrite is codenamed `spa`.

Naturally, some developers might want to use some of the composables used in the Settings app, seeing as they are built by Google itself. While Android is open source, adding the library directly from Google's repository would require syncing the entire Android framework, which has a lot of things unrelated to settings or UI.

This is where Salon comes in. It only contains code related to `spa` and its requirements. Salon also removes some restrictions on design to make it more compatible with 3rd party apps and their color/design schemes, while still adhering to Material 3 guidelines as much as possible, and adds support for older versions of Android.

## Versioning
As Salon is built out of 
Android's system code it uses a non-standard version scheme.

The version names have 2 version numbers - the first is for the system release, the second for the library release. For example, version name `14.0.0.r2-0.1` means that:
- this version has been built from the Android release with tag `android-14.0.0_r2`,
- the library version is `0.1`.

## Licensing
Salon is licensed under the Apache 2.0 license, which is also used by Android.
