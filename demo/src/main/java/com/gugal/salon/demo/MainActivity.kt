package com.gugal.salon.demo

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Slider
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.android.settingslib.spa.widget.preference.MainSwitchPreference
import com.android.settingslib.spa.widget.preference.SwitchPreferenceModel
import com.android.settingslib.spa.widget.ui.LinearProgressBar
import com.gugal.salon.SalonTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            var mainSwitch = remember {mutableStateOf(false)}
            SalonTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Column {
                        MainSwitchPreference(object : SwitchPreferenceModel {
                            override val title = "Open"
                            override val checked = mainSwitch
                            override val onCheckedChange: (Boolean) -> Unit =
                                { mainSwitch.value = it }
                        })
                        var pbHeight = remember{ mutableStateOf(4f)}
                        LinearProgressBar(progress = 0.5f, height = pbHeight.value)
                        Slider(pbHeight.value, {pbHeight.value=it}, valueRange = 1f..50f)
                    }
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    SalonTheme {
        Greeting("Android")
    }
}